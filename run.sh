#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

err() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  err "$msg"
  exit "$code"
}

usage() {
  local code=${1-0} # default exit status 1
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] -o output_dir --target target --test test

Wraps the performance of a target test in the setup and teardown specified by a
given target.  Requires that the target define __TEST_MOUNT and __TEST_UMOUNT
which set up and take down the target filesystem at a target directory (the sole
parameter). Output is placed in output_dir/test/target.

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-o              Output directory
--target        Name of target to test
--test          name of test to run
EOF
  exit "${code}"
}

parse_params() {
  # default values of variables set from params

  while :; do
    case "${1-}" in
    -h | --help) usage 0 ;;
    -v | --verbose) set -x ; VERBOSE="-v" ;;
    -o) # example named parameter
      [[ -z "${2-}" ]] && usage 1 #Otherwise this fails if you pass the flag but no parameter
      OUTPUT_DIR="${2-}"
      shift
      ;;
    --target) # example named parameter
      [[ -z "${2-}" ]] && usage 1 #Otherwise this fails if you pass the flag but no parameter
      TARGET="${2-}"
      shift
      ;;
    --test) # example named parameter
      [[ -z "${2-}" ]] && usage 1 #Otherwise this fails if you pass the flag but no parameter
      TEST="${2-}"
      shift
      ;;
    -?*) err "Unknown option: $1"; usage 1 ;;
    *) break ;;
    esac
    shift
  done

  # check required params and arguments
  [[ -z "${TARGET-}" ]] && die "Missing required parameter: target"
  [[ -z "${TEST-}" ]] && die "Missing required parameter: test"
  [[ -z "${OUTPUT_DIR-}" ]] && die "Missing required parameter: output_dir"

  return 0
}

parse_params "$@"

TEST_NAME=$(basename "$TEST")
TARGET_NAME=$(basename "$TARGET")

[ -f "$TARGET" ] || die "Target $TARGET does not exist"
source "$TARGET"
[ -v __TEST_MOUNT ] || die "$TARGET missing required variable: __TEST_MOUNT"
[ -v __TEST_UMOUNT ] || die "$TARGET missing required variable: __TEST_UMOUNT"

mkdir -p "$OUTPUT_DIR"/"$TEST_NAME"/"$TARGET_NAME"
TEST_MOUNTPOINT="$(mktemp -d)"

$__TEST_MOUNT "$TEST_MOUNTPOINT" #Note: $__TEST_MOUNT is unquoted because we want splitting so potential commands are interpreted correcty

"$TEST" ${VERBOSE-} -t "$TEST_MOUNTPOINT" -o "$OUTPUT_DIR"/"$TEST_NAME"/"$TARGET_NAME"

$__TEST_UMOUNT "$TEST_MOUNTPOINT" #Note: $__TEST_MOUNT is unquoted because we want splitting so potential commands are interpreted correcty

rmdir "$TEST_MOUNTPOINT"
