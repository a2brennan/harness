#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  # script cleanup here
}

err() {
  echo >&2 -e "${1-}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  err "$msg"
  exit "$code"
}

usage() {
  local code=${1-0} # default exit status 1
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] -o output_dir

Enumerates all possible pairs of targets/* and tests/* and invokes the run.sh
script to run trials, collating output in output_dir.

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-o              Output directory
EOF
  exit "${code}"
}

parse_params() {
  # default values of variables set from params

  while :; do
    case "${1-}" in
    -h | --help) usage 0 ;;
    -v | --verbose) set -x ; VERBOSE="-v" ;;
    -o)
      [[ -z "${2-}" ]] && usage 1 #Otherwise this fails if you pass the flag but no parameter
      OUTPUT_DIR="${2-}"
      shift
      ;;
    -?*) err "Unknown option: $1"; usage 1 ;;
    *) break ;;
    esac
    shift
  done

  # check required params and arguments
  [[ -z "${OUTPUT_DIR-}" ]] && die "Missing required parameter: -o output_dir"

  return 0
}

parse_params "$@"

SCRIPT_DIR=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)
TARGETS="$SCRIPT_DIR"/targets
TESTS="$SCRIPT_DIR"/tests

mkdir -p "$OUTPUT_DIR"
echo Commit $(git log | head -n 1 | awk '{ print $2; }') >> "$OUTPUT_DIR"/harness_config
git status >> "$OUTPUT_DIR"/harness_config
git diff >> "$OUTPUT_DIR"/harness_config

for TEST in "$TESTS"/*
do
    for TARGET in "$TARGETS"/*
    do
        "$SCRIPT_DIR"/run.sh ${VERBOSE-} --target "$TARGET" --test "$TEST" -o "$OUTPUT_DIR"
    done
done
